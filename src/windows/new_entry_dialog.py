from PySide2.QtWidgets import (QLineEdit, QPushButton, QApplication, QGridLayout, QLabel, QDialog)
from PySide2.QtCore import (Signal)
from PySide2.QtGui import (QIcon)

from src.util.logger import ActionLogger
from src.util.relative_path_handler import RelativePathHandler as rp
class NewEntryDialog(QDialog):
    entryFinished = Signal(dict)
    
    def __init__(self, requiredEntryFields, title="New entry", parent=None):
        super(NewEntryDialog, self).__init__(parent)
        #super().__init__(parent)
        
        self.log = ActionLogger('[NewEntryDialog]')
        self.requiredEntryFields = requiredEntryFields
        self.log.log_action("Starting new dialog")
        self.setWindowTitle(title)
        
        self.layout = QGridLayout()#QVBoxLayout()

        
        self.lineEditFields = {} # Initialize the entry fields that we will need to populate
        rowCount = 0
        for field in self.requiredEntryFields:
            entryLabel = QLabel(field)

            entryField = QLineEdit(self)
            entryField.setPlaceholderText(field)
            
            self.lineEditFields[field] = entryField
            self.layout.addWidget(entryLabel, rowCount, 0, 1, 1)
            self.layout.addWidget(entryField, rowCount, 1, 1, 1)
            rowCount += 1


        self.finishedButton = QPushButton("Add")
        self.finishedButton.setIcon( QIcon(rp.getIconPath('plus.svg') ))
        self.finishedButton.clicked.connect(self.finishedEnteringData)
        
        self.abortButton = QPushButton("Close")
        self.abortButton.clicked.connect(lambda : self.close())


        self.layout.addWidget(self.finishedButton)
        self.layout.addWidget(self.abortButton)

        self.setLayout(self.layout)

    def finishedEnteringData(self):
        formatedEntryList = {} # k,v dict with the values we entered
        for requiredRow in self.requiredEntryFields:
            formatedEntryList[requiredRow] = self.lineEditFields[requiredRow].text()

        self.entryFinished.emit(formatedEntryList)        




from PySide2 import QtWidgets
from PySide2 import QtGui

class MessageBox(QtGui.QWindow):    

    def __init__(self, message, title="Confirm Action", event=None):
        self.message = message
        self.title = title
        self.event = event
    
    def showDialog(self):
        messageBox = QtWidgets.QMessageBox()
        messageBox.setWindowTitle(self.title)
        messageBox.setText(self.message)
        messageBox.setStandardButtons(QtWidgets.QMessageBox.Close | QtWidgets.QMessageBox.Yes)
        messageBox.setDefaultButton(QtWidgets.QMessageBox.Close)
        messageBox.setIcon(QtWidgets.QMessageBox.Question)

        ret = messageBox.exec_()

        if ret == QtWidgets.QMessageBox.Close:
            if self.event:
                self.event.ignore()
            return False
        elif ret == QtWidgets.QMessageBox.Yes:
            if self.event:
                self.event.accept()
            return True
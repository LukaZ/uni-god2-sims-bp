import ntpath

class GenericUtil():

    @classmethod
    def getFileNameFromPath(cls, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)

    @classmethod
    def _removeTail(cls, text, suffix):
        if suffix and text.endswith(suffix):
            return text[:-len(suffix)]
        return text

    @classmethod
    def convertMetaFileNameToRawFileName(cls, filename, returnAsJson=False):
        if '/' in filename or not '_meta.json' in filename:
            raise NameError(f"Fajl nije ispravnog formata za konvertovanje iz meta u raw\nProsljedjeno: {filename}\nOcekivano: metaFile_meta.json")
        _clean = cls._removeTail(filename, '_meta.json')
        return _clean if not returnAsJson else _clean+'.json'
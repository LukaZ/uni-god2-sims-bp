import os


class RelativePathHandler(object):
    root = os.path.dirname(__file__).replace('/src/util','')
    iconsDir = root + '/assets/icons'
    dataDir = root + '/data'

    @classmethod
    def getRootDir(cls):
        return cls.root

    @classmethod
    def getDataDir(cls):
        return cls.dataDir

    @classmethod
    def getDatabasesDir(cls):
        return cls.getDataDir() + '/databases'

    @classmethod
    def getIconsDir(cls):
        return cls.iconsDir

    @classmethod
    def getIconPath(cls, icon:str):
        return f"{cls.getIconsDir()}/{icon}"
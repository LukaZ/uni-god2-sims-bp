from json import load, dump

class SimpleJSONLib():

    @classmethod
    def loadJSONFile(cls, path):
        try:
            result = {}
            with open(path, 'r') as fp:
                result = load(fp)
            return result
        except Exception as ex:
            print(repr(ex))

    @classmethod
    def saveJSONFile(cls, path, data):
        try:
            with open(path, 'w') as outfile:
                dump(data, outfile, indent=4 )
        except Exception as ex:
            print(repr(ex))

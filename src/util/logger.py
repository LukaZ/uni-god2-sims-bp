import traceback


class ActionLogger():
    
    def __init__(self, tag):
        self.tag = tag

    def log_action(self, action):
        print(f'{self.tag}: {action}')

    def handleError(self, ex):
        """
        Method to be called when a error occurs, in a production enviroment this would go nicely with sentry.io
        """
        print(f"{self.tag}: Encountered error during runtime:\n\t{str(repr(ex))}\n\tTrace:{traceback.print_exc()}")     
        
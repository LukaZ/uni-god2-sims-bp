from PySide2 import QtCore, QtGui, QtWidgets


#Util
from src.util.generic_util import GenericUtil
from src.util.logger import ActionLogger
from src.util.relative_path_handler import RelativePathHandler as rp
from src.util.simple_json import SimpleJSONLib

#Widgets
from src.widgets.menu_bar import MenuBar
from src.widgets.structure_dock import StructureDock
from src.widgets.workspace_widget import WorkspaceWidget
from src.widgets.database_explorer_dock import DatabaseExplorerDock
#Windows
from src.windows.message_box import MessageBox


class MainWindow(QtWidgets.QMainWindow):
    #dataHandler = None

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent=parent)
        
        self.metaDataFilePath = QtCore.QDir.currentPath() + '/data/meta'
        self.rawDataFilePath = QtCore.QDir.currentPath() + '/data/raw'

        self.workspaces = {}
        self.workspaceCount = 0

        self.setWindowTitle("Editor generickih podataka")
        self.log = ActionLogger('[MainWindow]')
       
    
    def setupUi(self, ui):
        """
        Add the needed functionality from the generated ui file.
        """
        
        self.structureDock = StructureDock("Structure dock", self)
        self.structureDock.newFileSelectedSignal.connect(self.handleNewFileSelectedSignal)
        self.ui = ui

        self.databaseExplorerDock = DatabaseExplorerDock(ui=self.ui)
        self.databaseExplorerDock.newDatabaseSelected.connect(self.handleNewDatabaseSelected)
        self.ui.databaseVerticalLayout.addWidget(self.databaseExplorerDock)

        self.menu_bar = MenuBar(self)
        self.setMenuBar(self.menu_bar)

        ui.mainTabWidget.tabCloseRequested.connect(self.removeWorkspaceTab )
        ui.structureDockGridLayout.addWidget(self.structureDock, 0, 0, 1, 1)

    def removeWorkspaceTab(self, index):
        currWidget = self.ui.mainTabWidget.currentWidget()
        if hasattr(currWidget, 'id'):
            self.workspaces.pop(currWidget.id)
        self.ui.mainTabWidget.removeTab(index)

    def handleNewFileSelectedSignal(self, path):
        self.log.log_action('New file was selected')
        fileName = GenericUtil.getFileNameFromPath(path)

        if path in self.workspaces.keys():
            self.log.log_action(f"Workspace {fileName} already loaded, canceling.")
            mbox = MessageBox("This workspace is already open, please switch to the approperiate tab.")
            mbox.showDialog()
            return
        
        newWorkspace = WorkspaceWidget(self, self.metaDataFilePath, self.rawDataFilePath, path)
        self.workspaces[path] = newWorkspace

        self.ui.mainTabWidget.addTab(self.workspaces[path],  QtGui.QIcon(rp.getIconPath('file.svg')), fileName)
        self.log.log_action(f'File \"{path}\" was selected and loaded!')
    
    def handleNewDatabaseSelected(self, treeWidgetRow):
        self.log.log_action("New database selected")
        

    def closeEvent(self, event):
        mbox = MessageBox("Are you sure you want to exit?",event=event)
        mbox.showDialog()

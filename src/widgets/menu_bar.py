
import webbrowser

from PySide2.QtCore import Signal, Slot
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QAction, QMenu, QMenuBar


class MenuBar(QMenuBar):

    def about_menu_action_view_source_code_click(self, action): # Zbog ovoga mi se nesvidja PEP8
        webbrowser.open('https://gitlab.com/LukaZ/uni-god2-sims-bp')


    def __init__(self, parent):
        super().__init__( parent )

        self.aboutMenu = QMenu("About", self)

        self.aboutMenuAction_viewSourceCode = QAction('View my source code', self)
        self.aboutMenuAction_viewSourceCode.triggered.connect(self.about_menu_action_view_source_code_click)
        self.aboutMenu.addAction(self.aboutMenuAction_viewSourceCode)

        self.addMenu(self.aboutMenu)


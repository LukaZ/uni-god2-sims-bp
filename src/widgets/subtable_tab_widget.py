from PySide2.QtWidgets import (QWidget, QHBoxLayout, QVBoxLayout, QSizePolicy, QSpacerItem, QPushButton)
from PySide2.QtCore import (QSize)
from PySide2.QtGui import ( QIcon )

from src.data_handlers.generic_subtable_data_handler import GenericSubtableDataHandler
from src.widgets.generic_table_model import GenericTableModel
from src.widgets.table_view import TableView
from src.widgets.table_view_tool_bar import TableViewToolBar

from src.util.logger import ActionLogger
from src.util.relative_path_handler import RelativePathHandler as rp

class SubTableTabWidget(QWidget):
    """
    Class that handles the inner workings of the subtables as the method inside workspaceWidget was getting too
    big for it's own good, also handles the "Load on demand" so as to not load large subtables as soon as the 
    end user clicks on a table row
    """
    def __init__(self, genericDataHandlerMaster, masterRowID:int, subtableName:str, parent=None):
        super().__init__(parent)

        self.genericDataHandlerMaster = genericDataHandlerMaster
        self.masterRowID = masterRowID
        self.subtableName = subtableName

        self.log = ActionLogger(f'[SubtableWidget][{subtableName}]')

        # Layouts to make sure the button is centered
        #self.gridLayout = QGridLayout()
        self.layout = QHBoxLayout()

        self.loadOnDemandButton = QPushButton("Load subtable data")
        self.loadOnDemandButton.setMinimumSize(QSize(150, 40))
        self.loadOnDemandButton.setIcon( QIcon(rp.getIconPath('log-in.svg') ))
        self.loadOnDemandButton.clicked.connect(self.beginLoad)

        self.horizontalSpacer_L = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.horizontalSpacer_R = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.layout.addItem(self.horizontalSpacer_L)
        self.layout.addWidget(self.loadOnDemandButton)
        self.layout.addItem(self.horizontalSpacer_R)

        self.setLayout(self.layout)
        self.log.log_action("Initialized")


    def clearLayout(self):
        """
        When beginLoad is called, have this method clear the placeholder button for requesting loading
        (You'd thing there would be a easier way of doing this) 
        """
        for i in reversed(range(self.layout.count())): 
            item = self.layout.itemAt(i)
            widget = item.widget()
            if widget != None:
                self.log.log_action("Deleted one widget")
                self.layout.removeWidget(widget)
                widget.setParent(None) # Avoid seg fault 
                #widget.deleteLater()#.setParent(None)
            elif widget == None and item != None:
                self.log.log_action("Deleted one item")
                self.layout.removeItem(item)
                item = None
        

    def beginLoad(self):
        """
        Method that starts the process of loading the data
        """
        self.log.log_action("Load demanded, loading...")
        
        # The actual loading of all the data
        masterRowData = self.genericDataHandlerMaster.get_one(self.masterRowID) 
        rowData = masterRowData[self.subtableName]
        
        self.genericSubtableDataHandler = GenericSubtableDataHandler(rowData, self.masterRowID, self.subtableName)
        self.genericSubtableDataHandler.updateMasterRowData.connect(self.saveSubtableData)
        self.tableModel = GenericTableModel(self.genericSubtableDataHandler, self)
        self.tableView = TableView(self)
        self.tableView.setModel(self.tableModel)
        self.tableViewToolBar = TableViewToolBar(self.tableView, self.genericSubtableDataHandler)

        self.clearLayout()
        
        # Ovaj deo je bio nepotrebno komplikovan
        QWidget().setLayout(self.layout)
        layout = QVBoxLayout()
        layout.addWidget(self.tableViewToolBar)
        layout.addWidget(self.tableView)
        self.setLayout(layout)
        self.layout = layout

        self.log.log_action("Finished loading!")
        


    def saveSubtableData(self, subtablePK, masterRowID, subtableData):
        # Ovde razmenjujemo dosta parametara koji nam inace netrebaju jer ih vec imamo lokalno
        self.log.log_action(f"Updating and saving a row in subtable {subtablePK} with row id {masterRowID}")
        parentRow = self.genericDataHandlerMaster.get_one(masterRowID)
        parentRow[subtablePK] = subtableData
        #self.handler.data['raw'][masterRowID] = parentRow
        self.genericDataHandlerMaster.edit(parentRow, masterRowID)



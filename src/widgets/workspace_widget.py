#from PySide2.QtWidgets import QVBoxLayout, QTableView, QAbstractItemView, QTabWidget, QWidget
from PySide2 import QtWidgets
from PySide2.QtGui import QIcon

from src.widgets.generic_table_model import GenericTableModel
from src.widgets.table_view import TableView
from src.widgets.table_view_tool_bar import TableViewToolBar
from src.widgets.subtable_tab_widget import SubTableTabWidget

from src.util.logger import ActionLogger
from src.util.relative_path_handler import RelativePathHandler as rp
from src.util.generic_util import GenericUtil

from src.data_handlers.generic_data_handler import GenericDataHandler
from src.data_handlers.generic_subtable_data_handler import GenericSubtableDataHandler
#from src.data_handlers.subtable_generica_data_handler import SubtableGenericDataHandler
from src.data_handlers.serial_data_handler import SerialDataHandler
from src.data_handlers.sequential_data_handler import SequentialDataHandler

class WorkspaceWidget(QtWidgets.QWidget):

    def __init__(self, parent, metaDataFilePath, rawDataFilePath, id):
        super().__init__(parent)
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.tabWidget = None
        self.id = id # Used to identify the workspace widget in the tabs 

        # k, v values for subtables , k is the int index of the row in the main table, v the data itself
        self.subtables = {} 
        
        self.log = ActionLogger(f"[WorkspaceWidget][{GenericUtil.getFileNameFromPath(id)}]")
        self.handler = GenericDataHandler(metaDataFilePath, rawDataFilePath, id, asJSON=True)

        self.createTabWidget()
        self.createMainTableWidget()
        self.populateTable(self)

        self.mainLayout.addWidget(self.toolBar)
        self.mainLayout.addWidget(self.mainTable)
        self.mainLayout.addWidget(self.tabWidget)
        self.setLayout(self.mainLayout)

    def createMainTableWidget(self):
        self.mainTable = TableView(self)
        self.mainTable.clicked.connect(self.mainTableRowSelected)
        self.toolBar = TableViewToolBar(self.mainTable, self.handler)

    def saveSubtableData(self, subtablePK, masterRowID, subtableData):
        self.log.log_action(f"Updating and saving a row in subtable {subtablePK} with row id {masterRowID}")
        parentRow = self.handler.get_one(masterRowID)
        parentRow[subtablePK] = subtableData
        #self.handler.data['raw'][masterRowID] = parentRow
        self.handler.edit(parentRow, masterRowID)
        
    def createSubtableWidget(self, index, closeExistingTabsOnNew=True):
        self.log.log_action("Creating subtable")
        rowID = index.row()
        
        if not closeExistingTabsOnNew:
            if rowID in self.subtables.keys():
                self.log.log_action("Widget with this subtable already exists!")
                return
        else:
            self.tabWidget.clear()

        for subtableColumn in self.handler.getSubtables(): # person: {}, extra: {}
            subtable = SubTableTabWidget(self.handler, rowID, subtableColumn, parent=self.tabWidget)
            self.subtables[rowID] = subtable
            self.tabWidget.addTab(self.subtables[rowID], subtableColumn)
          

    def mainTableRowSelected(self, index):
        if self.handler.hasSubtable():
            self.log.log_action("Has subtable!")
            self.createSubtableWidget(index)


    def populateTable(self, parrent):            
        self.log.log_action(f"Populating table with {len(self.handler.data['raw'])} raw entries")

        self.tableModel = GenericTableModel( self.handler, parent=parrent)
        self.mainTable.setModel(self.tableModel)

    def createTabWidget(self):
        self.tabWidget = QtWidgets.QTabWidget()
        self.tabWidget.setTabsClosable(True)
  
        self.tabWidget.tabCloseRequested.connect(self.deleteTabWidget)


    def deleteTabWidget(self, index):
        self.tabWidget.removeTab(index)
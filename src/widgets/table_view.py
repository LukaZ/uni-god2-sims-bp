from PySide2 import QtWidgets
from PySide2 import QtGui



#from PySide2.QtWidgets import QMessageBox

from src.util.logger import ActionLogger
from src.windows.message_box import MessageBox

class TableView(QtWidgets.QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)   
        
        self.log = ActionLogger('[TableView]')
        
        self.deleteRowAct = QtWidgets.QAction("Delete row",self)
        self.deleteRowAct.setIcon(QtGui.QIcon('./assets/icons/delete.svg'))
        #self.deleteRowAct.triggered.connect(self.deleteRow)
        

    def deleteRow(self, row, col):
        mbox = MessageBox("Are you sure you want to delete this row?")
        response = mbox.showDialog()
 
        if response:
            self.log.log_action(f"Deleting row {row}")
            #self.model().getGenericDataHandler().delete_one(row) # Delete the actuall data
            self.model().deleteRow(row) # Delete row in the visual table
            
    def getcurrentSelectedRow(self):
        return self.selectionModel().currentIndex().row()

    def contextMenuEvent(self, event):
        row = self.rowAt(event.y())
        column = self.columnAt(event.x())

        menu = QtWidgets.QMenu(self)
        self.deleteRowAct.triggered.connect(lambda: self.deleteRow(row,column))
        menu.addAction(self.deleteRowAct)
        menu.exec_(event.globalPos())


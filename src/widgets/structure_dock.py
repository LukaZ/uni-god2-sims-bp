from PySide2 import QtWidgets, QtGui, QtCore


class StructureDock(QtWidgets.QDockWidget):
    newFileSelectedSignal = QtCore.Signal(str)

    def __init__(self, title, parent):
        super().__init__(title, parent)

        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(QtCore.QDir.currentPath())
        
        self.tree = QtWidgets.QTreeView()
        self.tree.setModel(self.model)
        self.tree.setRootIndex(self.model.index(QtCore.QDir.currentPath() + '/data/meta'))
        self.tree.clicked.connect(self.file_clicked)
        self.tree.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.tree.geometry().setWidth(200)

        self.setWidget(self.tree)

    def file_clicked(self, index):
        _filePath = self.model.filePath(index)
        self.newFileSelectedSignal.emit(_filePath)


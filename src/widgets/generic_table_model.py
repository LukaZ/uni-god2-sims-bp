from PySide2 import QtCore
from PySide2.QtCore import QModelIndex
from src.util.logger import ActionLogger

class GenericTableModel(QtCore.QAbstractTableModel):
    rowEditedSignal = QtCore.Signal(object)

    def __init__(self, gdh, parent=None):
        super().__init__(parent)
        self.log = ActionLogger("[GenericTableModel]")
        self.tableData = {}

        self.parent = parent
        self.gdh = gdh
        self.row_count = None

    def deleteRow(self, row):
        self.gdh.delete_one(row) # Delete the actuall data

    def getGenericDataHandler(self):
        return self.gdh

    def getElement(self, index):
        return self.gdh.get_one(index.row())

    def rowCount(self, index=None):
        return self.gdh.getRowCount()

    def columnCount(self, index):
        return len(self.gdh.getMetaDataColumns())

    def data(self, index, role):
        try:
            if role == QtCore.Qt.DisplayRole:
                if self.gdh.getMetaData()['type'] == 'subtable':
                    pass
                    #print("Here!")
                data = self.getElement(index)
                
                returnData = data[self.gdh.getMetaDataColumns()[index.column()]]
                if data == None: return {}
                #if type(data) != dict or type(data) != list : return data
                return returnData
            
            elif role == QtCore.Qt.EditRole:
                columnID = index.column()
                rowData = self.gdh.get_one(index.row()) 
                editedColumn = self.gdh.getMetaDataColumns()[columnID]
                editedValue = rowData.get(editedColumn)
                return editedValue

        except Exception as ex:
            self.log.handleError(ex) 
        

    def headerData(self, section, orientation, role):
        try:
                
            if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
                return self.gdh.getMetaDataColumns()[section]
            return None
        except Exception as ex:
            self.log.handleError(ex)


    #metode za editable model
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        try:
            rowData = self.getElement(index)
            if role == QtCore.Qt.EditRole:
                rowData[self.gdh.getMetaDataColumns()[index.column()]] = value
                return self.gdh.edit(rowData, index)

        except Exception as ex:
            self.log.handleError(ex) 
            
    def flags(self, index):
        return super().flags(index) | QtCore.Qt.ItemIsEditable # ili nad bitovima

    
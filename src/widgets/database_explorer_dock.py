from PySide2.QtCore import QSize, QModelIndex, Signal
from PySide2.QtWidgets import ( QTreeWidget, QTreeWidgetItem, QTableView, QHeaderView, QWidget, 
    QVBoxLayout, QTabWidget)
from PySide2.QtGui import QStandardItem, QStandardItemModel, QIcon
from PySide2.QtSql import QSqlRelationalDelegate, QSqlRelationalTableModel

from src.data_handlers.generic_database_handler import GenericDatabaseHandler
from src.data_handlers.database_data_handler import DatabaseDataHandler

from src.util.relative_path_handler import RelativePathHandler as rp
from src.util.simple_json import SimpleJSONLib
from src.util.logger import ActionLogger

from src.windows.message_box import MessageBox

from src.widgets.table_view_tool_bar import TableViewToolBar
from src.widgets.table_view import TableView

class DatabaseExplorerDock(QTreeWidget):
    newDatabaseSelected = Signal(object)

    def __init__(self, ui=None):
        super().__init__()
        
        self.ui = ui
        self.log = ActionLogger('[DatabaseExplorerDock]')
        self.setMinimumSize(QSize(0,250))
        self.setAlternatingRowColors(True)
        
        #self.databaseTreeView_Model = Q#QStandardItemModel()
        #self.databaseTreeView_Model.setHorizontalHeaderLabels(['Name','Host','Port'])
        #self.setModel(self.databaseTreeView_Model)
        self.setHeaderLabels(['Name', 'Host', 'Port'])
        self.itemDoubleClicked.connect(self.handleNewItemSelected)

        self.handlers = []    
        self.populateAvailableDatabases()
        
        
    def populateAvailableDatabases(self):
        self.log.log_action("Populating available databases...")

        self.databaseJSON = SimpleJSONLib().loadJSONFile(f'{rp.getDatabasesDir()}/databases.json')
        self.log.log_action(f"Loaded databases JSON file, {len(self.databaseJSON)} entries")

        _rowCount = 0
        for database in self.databaseJSON:
            databaseServer_Handler = GenericDatabaseHandler(
                database['host'],
                database['port'],
                database['db'],
                database['username'],
                database['password']
            )
            #databaseServer_TreeViewRow = QStandardItem(f"{database['name']} {database['host']} {database['port']}")
            databaseServer_TreeViewRow = QTreeWidgetItem(self)
            databaseServer_TreeViewRow.setText(0, str(database['name']))
            databaseServer_TreeViewRow.setText(1, str(database['host']))
            databaseServer_TreeViewRow.setText(2, str(database['port']))
            databaseServer_TreeViewRow.databaseHandler = databaseServer_Handler
            databaseServer_TreeViewRow.setIcon(0, QIcon( rp.getIconPath('database.svg')))

            # database_LoadTables = QTreeWidgetItem(self)
            # database_LoadTables.setText(0, "Double click to load tables...")


            self.addTopLevelItem(databaseServer_TreeViewRow)
            self.handlers.append(databaseServer_TreeViewRow)
            #self.databaseTreeView_Model.appendRow(databaseServer_TreeViewRow)
            
            #self.setFirstColumnSpanned(_rowCount, self.rootIndex(), True)
            self.setColumnWidth(0, 150)    
            _rowCount += 1
        #self.resizeColumnToContents(0)
        
    def populateTablesInsideDatabase(self, treeWidgetItem):
        """
        Querry the database and obtain all the 
        """
        tables = treeWidgetItem.databaseHandler.getMetaDataColumns()

        for table in tables:
            _tableFks = treeWidgetItem.databaseHandler.getForeignKeysInTable(table)
            _tableMetaData = treeWidgetItem.databaseHandler.getTableMetaData(table)
            
            _table = QTreeWidgetItem(treeWidgetItem)
            _table.databaseTableData = {
                'name':table,
                'foreignRelations': _tableFks,
                'tableMetaData':_tableMetaData
            }
            _table.masterHandler = treeWidgetItem.databaseHandler # So that we can send querries in the tables 
            _table.setText(0,table)
            _table.setIcon(0, QIcon( rp.getIconPath('columns.svg')))

    

    def handleNewItemSelected(self, treeWidgetItem):
        if hasattr(treeWidgetItem, "databaseHandler"): # We clicked on a database
            self.log.log_action("New database was selected!")
            if treeWidgetItem.databaseHandler.initialize_backend_data_handler():
                self.log.log_action('Connection ok!')
                self.populateTablesInsideDatabase(treeWidgetItem)
            else:
                mbox = MessageBox("Unable to connect to database")
                mbox.showDialog()

            self.newDatabaseSelected.emit(treeWidgetItem)
        else: # We clicked on a table
            self.log.log_action("New table was selected!")
            tableName = treeWidgetItem.databaseTableData['name'] # 
            tableMetaData = treeWidgetItem.databaseTableData['tableMetaData']
            tableForeignRelations = treeWidgetItem.databaseTableData['foreignRelations']

            dummyLayoutHolder = QWidget()
            layout = QVBoxLayout(dummyLayoutHolder)

            model = DatabaseDataHandler(tableName, tableForeignRelations, tableMetaData, treeWidgetItem.masterHandler)

            tableView = TableView(dummyLayoutHolder)
            tableView.setModel(model)
            tableView.setItemDelegate(QSqlRelationalDelegate(tableView))
            tableView.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)  
            tableView_ToolBar = TableViewToolBar(tableView, model, isDatabaseToolBar=True)
            
            # Trik koji sam otkrio za brisanje sa OID uz minimalnu izmenu koda za brisanje kod baza, 
            # TL;DR za brisanje generisemo OID's (https://www.postgresql.org/docs/8.1/datatype-oid.html)
            # i onda ih naknadno ( linija ispod ovog komentara ) sakrijemo kolonu 0 ( Kolona sa OID's ), potom kod deleteEntryTrigger u
            # table_view_tool_bar.py indeksiramo kolonu 0 i selektovani red, iako je kolona sakrivena, ona idalje postoji kao objekat,
            # onda samo obrisemo kolonu sa OID==rowToDelete =) ; Ovime izbegavamo glavobolju sa rad sa kardinalitetom kod kompozitnih PK's 
            tableView.setColumnHidden(0,True) 
            
            layout.addWidget(tableView_ToolBar)
            layout.addWidget(tableView)

            subtableMoshPit = QTabWidget(dummyLayoutHolder)
            layout.addWidget(subtableMoshPit)

            dummyLayoutHolder.setLayout(layout) 

            self.ui.mainTabWidget.addTab(dummyLayoutHolder, QIcon( rp.getIconPath('columns.svg')), tableName)

            
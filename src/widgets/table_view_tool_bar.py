from PySide2.QtWidgets import QToolBar, QTableView, QAction, QPushButton
from PySide2.QtGui import QIcon
from PySide2.QtCore import QSize

from src.util.relative_path_handler import RelativePathHandler as rp
from src.util.logger import ActionLogger
from src.windows.new_entry_dialog import NewEntryDialog
from src.windows.message_box import MessageBox

class TableViewToolBar(QToolBar):

    def __init__(self, linkedTableView, genericDataHandler, isDatabaseToolBar=False, parent=None):
        super().__init__(parent)

        self.linkedTableView = linkedTableView
        self.genericDataHandler = genericDataHandler
        self.isDatabaseToolBar = isDatabaseToolBar

        self.log = ActionLogger('[TableViewToolBar]')
        # Ovaj kod se moze vise modularizovati ali onda je vec estetika u pitanju ne funkcionalnost TODO

        self.addEntry = QPushButton('Add data', parent=linkedTableView)
        self.addEntry.setIcon( QIcon(rp.getIconPath('plus.svg') ))
        self.addEntry.setIconSize(QSize(25,25))
        self.addEntry.clicked.connect(self.addEntryTrigger)
        self.deleteEntry = QAction(QIcon(rp.getIconPath('minus.svg')), "Delete Selected", parent=linkedTableView)
        self.deleteEntry.triggered.connect(self.deleteEntryTrigger)
        self.saveData = QAction(QIcon(rp.getIconPath('save.svg')), "Save data", parent=linkedTableView)
        self.saveData.triggered.connect(self.saveDataTrigger)
        self.jumpUp = QAction(QIcon(rp.getIconPath('chevrons-up.svg')), "Jump to top", parent=linkedTableView)
        self.jumpUp.triggered.connect(self.jumpUpTrigger)
        self.goUp = QAction(QIcon(rp.getIconPath('chevron-up.svg')), "Up", parent=linkedTableView)
        self.goUp.triggered.connect(self.goUpTrigger)
        self.goDown = QAction(QIcon(rp.getIconPath('chevron-down.svg')), "Down", parent=linkedTableView)
        self.goDown.triggered.connect(self.goDownTrigger)
        self.jumpDown = QAction(QIcon(rp.getIconPath('chevrons-down.svg')), "Jump to bottom", parent=linkedTableView)
        self.jumpDown.triggered.connect(self.jumpDownTrigger)
        self.refresh = QAction(QIcon(rp.getIconPath('refresh-cw.svg')), "Refresh", parent=linkedTableView)
        self.refresh.triggered.connect(self.refreshTrigger)
        
        #self.addAction(self.addEntry)
        self.addWidget(self.addEntry)
        self.addAction(self.deleteEntry)
        self.addAction(self.saveData)
        self.addAction(self.jumpUp)
        self.addAction(self.goUp)
        self.addAction(self.goDown)
        self.addAction(self.jumpDown)
        self.addAction(self.refresh)


    def addEntryTrigger(self):
        dialog = NewEntryDialog(self.genericDataHandler.getMetaDataColumns(), parent=self.linkedTableView)
        
        def _finishedEnteringData(data):
            if self.genericDataHandler.insert(data):
                self.log.log_action("Finished adding in new data")
                self.genericDataHandler.save_rawdata()
            else:
                mbox = MessageBox('Error inserting new data',title="Error inserting new data")
                mbox.showDialog()
            dialog.close()

        self.log.log_action("Showing new entry dialog")
        dialog.entryFinished.connect(_finishedEnteringData)
        dialog.show()
        

    def deleteEntryTrigger(self):
        _displayName = None
        rowToDelete = rowToDelete = self.linkedTableView.getcurrentSelectedRow()
        if self.isDatabaseToolBar:
            index = self.linkedTableView.rowAt(rowToDelete)
            oid = self.linkedTableView.model().index(rowToDelete, 0).data() # Kolona 0 je OID, 1 je "Display" ime
            _displayName = self.linkedTableView.model().index(rowToDelete, 1).data() # Kolona 0 je OID, 1 je "Display" ime
            rowToDelete = oid
            
        
            
        mbox = MessageBox(f'Are you sure you want to delete row ID {rowToDelete if not self.isDatabaseToolBar else _displayName}') 
        if mbox.showDialog():
            self.log.log_action(f"Deleting row {rowToDelete}")
            if self.genericDataHandler.delete_one(rowToDelete):
                mbox = MessageBox(f'Successfully deleted row {rowToDelete}',title="Action finished")
                mbox.showDialog()
                

    def saveDataTrigger(self):
        self.log.log_action("Saving data")
        self.genericDataHandler.save_rawdata()


    def jumpUpTrigger(self):
        self.linkedTableView.selectRow(0)
        #self.linkedTableView.currentSelectedRow = 0

    def goUpTrigger(self):
        #self.linkedTableView.currentSelectedRow = self.linkedTableView.currentSelectedRow - 1 if self.linkedTableView.currentSelectedRow > 0 else 0
        current = self.linkedTableView.getcurrentSelectedRow()
        toSelect = current - 1 if current - 1 >= 0 else 0
        self.log.log_action(f"Selecting row {toSelect}")
        self.linkedTableView.selectRow(toSelect)

    def goDownTrigger(self):
        current = self.linkedTableView.getcurrentSelectedRow()
        toSelect = current + 1 if current + 1 <= self.linkedTableView.model().rowCount() else current
        self.log.log_action(f"Selecting row {toSelect}")
        self.linkedTableView.selectRow(toSelect)


    def jumpDownTrigger(self):
        count = self.linkedTableView.model().rowCount() - 1
        self.log.log_action(f"Selecting row {count}")
        self.linkedTableView.selectRow(count)

    def refreshTrigger(self):
        self.linkedTableView.update()
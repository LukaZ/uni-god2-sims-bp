import pickle
import collections
from src.util.logger import ActionLogger
from src.data_handlers.data_handler import DataHandler


class SequentialDataHandler(DataHandler): # {'pk':data, 'pk': data }
    
    def __init__(self, metaData, rawData):
        super().__init__()
        
        self.log = ActionLogger('[SequentialDataHandler]')
        
        self.data = {}
        self.data['meta'] = metaData
        self.data['raw'] = rawData

        # Make sure we can support unorganized lists
        if type(self.data['raw']) != dict:
            # Sequential has a PK, sort the list 
            self.log.log_action("Formating dictionary...")
            formedDictKV = {}
            for entry in self.data['raw']:
                formedDictKV[ entry[ self.data['meta']['pk'] ] ] = entry
            
            self.log.log_action("Sorting raw data...")
            self.data['raw'] = dict(sorted(formedDictKV.items()))
        
            self.log.log_action("Sorting done!")
        else:
            self.log.log_action("Looks like the data is already sorted")
    
    def get_pk(self):
        return self.data['meta']['pk']

    def get_object_index(self, id:int):
        return list(self.data['raw'].keys())[id]

    def get_handler_type(self):
        return 'sequential'
    
    def get_one(self, id:int):
        pk = self.get_object_index(id)
        return self.data['raw'][pk]
    
    def get_all(self):
        return self.data['raw']

    def edit(self, newObj, index):
        self.log.log_action("Editing to new value")
        try:
            entryPk = self.data['meta']['pk'] # [name] == Afghanistan
            rawDataIndex = newObj[entryPk]
            self.data['raw'][rawDataIndex] = newObj
            return True
        except Exception as ex:
            return False

    def delete_one(self, id:int):
        pk = self.get_object_index(id)
        self.data['raw'].pop(pk)
        self.log.log_action(f"Deleted {id} \"{pk}\"")

    def insert(self, obj) -> bool:
        pk = self.get_pk() # 'name'
        newObjPk = obj[pk]
        if newObjPk not in self.data['raw'].keys():
            self.data['raw'][newObjPk] = obj
            return True
        else:
            return False
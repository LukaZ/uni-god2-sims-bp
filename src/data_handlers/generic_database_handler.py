from PySide2.QtSql import *
from PySide2.QtGui import QStandardItem, QStandardItemModel, QIcon

#import mysql.connector
import pymysql
from src.util.logger import ActionLogger

class GenericDatabaseHandler():

    def __init__(self, host, port, db, username, hashPwd):
        self.host = host
        self.port = port
        self.dbName = db
        self.username = username
        self.hashedPassword = hashPwd
        self.log = ActionLogger(f'[GenericDatabaseHandler][{self.host}:{self.port}]')

    
    def initialize_backend_data_handler(self):
        self.log.log_action("Starting connection...")
        try:
            db = QSqlDatabase.addDatabase("QPSQL")
            db.setHostName(self.host)
            db.setPort(self.port)
            #db.setDatabaseName(self.dbName)
            db.setUserName(self.username)
            db.setPassword(self.hashedPassword)
            self.db = db
            ok = self.db.open()

            self.isLive = ok
            return self.isLive
        except Exception as ex:
            self.log.handleError(ex)
            return False

    def isMetaDataValid(self):
        return True

    def getTablePKs(self, tableName:str):
        self.log.log_action(f"Getting {tableName} primary key...")
        query = QSqlQuery()
        query.prepare("""
            SELECT               
                pg_attribute.attname, 
                format_type(pg_attribute.atttypid, pg_attribute.atttypmod) 
                FROM pg_index, pg_class, pg_attribute, pg_namespace 
            WHERE 
                pg_class.oid = :tableName::regclass AND 
                indrelid = pg_class.oid AND 
                nspname = 'public' AND 
                pg_class.relnamespace = pg_namespace.oid AND 
                pg_attribute.attrelid = pg_class.oid AND 
                pg_attribute.attnum = any(pg_index.indkey)
            AND indisprimary
            """
        )
        query.bindValue(':tableName', tableName)
        query.exec_()
        columns = {}
        count = 0
        while query.next():
            colName = query.value(0)
            colDataType = query.value(1)
            
        return columns

    def delete_one(self, oid:int, tableName:str):
        commandToExecute = f'DELETE FROM public.{tableName} WHERE OID = {oid}'
        query = QSqlQuery(commandToExecute)
        # query.prepare(commandToExecute)
        # query.bindValue(':tableName', tableName)
        # query.bindValue(':oid',oid)
        query.exec_()
        return True

    def getTableMetaData(self, tableName:str):
        """
        Returns a dictionary that contains column names aswell as the column data type
        """

        self.log.log_action("Getting columns inside table...")
        query = QSqlQuery()
        query.prepare("""
            SELECT  
                column_name,
                data_type
            FROM 
                information_schema.columns
            WHERE 
                table_name = :tableName;
            """
        )
        query.bindValue(':tableName', tableName)
        query.exec_()
        columns = {}
        count = 0
        while query.next():
            colName = query.value(0)
            colDataType = query.value(1)
            columns[colName] = {
                'numericIndex':count,
                'dataType':colDataType
            }
            count += 1
        return columns

    def getMetaDataColumns(self):
        self.log.log_action("Getting tables inside database...")
        # Napomena o bezbednosti: Information_schema u produkciji nesme biti dostupno nalogu dodeljenom za aplikaciju ili
        # bilo sta sto je van operativne baze podataka   
        query = QSqlQuery()
        query.prepare("""
                SELECT table_name FROM information_schema.tables
                WHERE table_schema = :dbName"""
        )
        query.bindValue(':dbName', self.dbName)
        query.exec_()
        tables = []
        while query.next():
            tableName = query.value(0)
            tables.append(tableName)
        return tables


    def getForeignKeysInTable(self, table:str):
        self.log.log_action("Getting foreign key columns inside database...")
        query = QSqlQuery()
        query.prepare("""
        SELECT DISTINCT ON ( kcu.constraint_name )
            tc.table_schema, 
            tc.constraint_name, 
            tc.table_name, 
            kcu.column_name, 
            ccu.table_schema AS foreign_table_schema,
            ccu.table_name AS foreign_table_name,
            ccu.column_name AS foreign_column_name 
        FROM 
            information_schema.table_constraints AS tc 
            JOIN information_schema.key_column_usage AS kcu
            ON tc.constraint_name = kcu.constraint_name
            AND tc.table_schema = kcu.table_schema
            JOIN information_schema.constraint_column_usage AS ccu
            ON ccu.constraint_name = tc.constraint_name
            AND ccu.table_schema = tc.table_schema
        WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=:tableToSearch;
        """)
        query.bindValue(':tableToSearch', table)
        query.exec_()
        foreignKeysInTable = []
        while query.next():
            _constraint = {}
            _constraint['constraintName'] = query.value('constraint_name')
            _constraint['foreignTableName'] = query.value('foreign_table_name')
            _constraint['foreignColumnName'] = query.value('foreign_column_name')
            #_constraint['displayColumn'] = 'naziv'

            foreignKeysInTable.append(_constraint)
        return foreignKeysInTable

    def columnCount(self):
        return len(self.getMetaDataColumns())

    # @abstractmethod
    # def getRowCount(self):
    #     pass
    
    # @abstractmethod
    # def insert(self, obj):
    #     pass


    # ###
    # @abstractmethod
    # def get_one(self, id):
    #     pass
    
    # @abstractmethod
    # def get_all(self):
    #     pass

    # @abstractmethod
    # def edit(self, obj, index):
    #     pass

    # @abstractmethod
    # def delete_one(self, id):
    #     pass

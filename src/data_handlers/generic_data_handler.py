import pickle
from src.util.logger import ActionLogger
from src.data_handlers.data_handler import DataHandler
from src.util.simple_json import SimpleJSONLib
from src.util.generic_util import GenericUtil

from src.data_handlers.serial_data_handler import SerialDataHandler
from src.data_handlers.sequential_data_handler import SequentialDataHandler

class GenericDataHandler():

    def __init__(self, metaDataPath, rawDataPath, metaFilePath, asJSON=False):
        super().__init__()
        self.log = ActionLogger('[GenericDataHandler]')
        self.asJSON = asJSON

        #Setting the apropperiate paths 
        self.metaDataPath = metaDataPath # putanja do metadata foldera #$PROJECT/data/meta
        self.rawDataPath = rawDataPath # putanja do raw data foldera ( Binarni fajlovi ) # $PROJECT/data/raw
             
        self.metaFilePath = metaFilePath # $PROJECT/data/meta/isocodes_meta.json
        self.metaFile = self.metaFilePath[ self.metaFilePath.rfind("/")+1 : ] # ime samog metafajla bez putanje ie. 'iso_codes_meta.json'
        
        # Data loading 
        self.data = {}
        self.data['meta'] = self.load_metadata(self.metaFilePath)

        self.rawFile = self.data['meta']['sourceFile']
        self.rawFilePath = f'{self.rawDataPath}/{self.rawFile}'

        self.data['raw'] = self.load_rawdata(self.rawFilePath)
        
        # WARN DO NOT DELETE in case pickle gets corrupted, run this
        #self.save_rawdata(self.rawDataPath +'/'+ self.rawFile.replace(".json",""))
        
        self.initialize_backend_data_handler()

        # In case we formatted the data in some way since it was loaded 
        self.data['raw'] = self.backendDataService.get_all()

        self.log.log_action("Done initializing new GenericDataHandler!")


    def getSubtables(self):
        if not self.hasSubtable(): raise Exception("Row does not have subtables!")
        return self.data['meta']['subtables']

    def hasSubtable(self):
        return 'subtables' in self.data['meta'] and len(self.data['meta']['subtables']) > 0

    def getRowCount(self):
        return len(self.data['raw'])

    def getMetaDataColumns(self):
        return self.data['meta']['columns']

    def getMetaData(self):
        return self.data['meta']

    def get_handler_type(self):
        return self.data['meta']['type']

    def get_one(self, id:int):
        return self.backendDataService.get_one(id)
    
    def get_all(self):
        self.log.log_action("Returning all values")
        return self.backendDataService.get_all()

    def edit(self, newData, index):
        self.log.log_action("Editing cell to new value")
        if self.backendDataService.edit(newData, index):
            self.log.log_action("Editing finished, saving new data...")
            self.save_rawdata(self.rawFilePath)
            return True
        else:
            self.log.handleError(Exception("Could not edit cell data, errors occured!"))
        return False

    def delete_one(self, id):
        self.backendDataService.delete_one(id)
        self.save_rawdata(self.rawFilePath, data=self.backendDataService.get_all())
        return True

    def insert(self, obj):
        self.log.log_action("Inserting new data")
        return self.backendDataService.insert(obj)
    
    def load_rawdata(self, path):
        self.log.log_action('Loading raw data')
        if self.asJSON:
            if GenericUtil.getFileNameFromPath(path).find(".json") == -1:
                raise Exception("Specified file to load is not a JSON file extension!")
            self.log.log_action("[WARN] Loading a json file not a pickled binary file!")
            
            return SimpleJSONLib.loadJSONFile(path)
        else:    
            with open(path, 'rb') as dfile:
                data = pickle.load(dfile) #koristimo pickle za deserijalizaciju podataka
                return data


    def save_rawdata(self, path=None, data=None):
        if path == None: path = self.rawFilePath
        if data == None: data = self.data['raw']
        self.log.log_action(f"Saving raw {('JSON'if self.asJSON else '')} data into path {path}")
        try:
            if self.asJSON:
                SimpleJSONLib.saveJSONFile(path, data)
            else:
                with open(path, 'wb') as data_file:
                    pickle.dump(data, data_file) #koristimo pickle da bismo serijalizovali u binarnu datoteku
            self.log.log_action(f"Saved raw data to path {path} "+("as json!" if self.asJSON else "!") )
        except Exception as ex:
            self.log.handleError(ex)


    def isMetaDataValid(self, metaData):
        """
        Checks if the loaded metadata contains all the required keys
        """
        isValid = True
        _requiredKeys = [
            'sourceFile',
            'type',
            #'pk',
            'columns'
        ]
        
        for key in _requiredKeys:
            if key not in metaData:
                isValid = False
                
        return isValid
        

    def load_metadata(self, path, performValidityCheck_Meta=True): 
        self.log.log_action('Loading metadata')

        data = SimpleJSONLib.loadJSONFile(path)
        
        return data if self.isMetaDataValid(data) else self.log.handleError(Exception("Metadata fajl nesadrzi odgovarajuce kljuceve!"))

    def save_metadata(self, path, data): # Unused ? 
        SimpleJSONLib.saveJSONFile(path,data)

    def initialize_backend_data_handler(self):
        """
        After we are finished loading the meta and raw data this method takes care of initializing the apropperiate class
        for handling the data aproperiatelly 
        """
        self.log.log_action("Initializing backend for data")

        self.handlerType = self.get_handler_type()
        self.backendDataService = None
        
        if self.handlerType == 'serial':
            self.backendDataService = SerialDataHandler(self.data['meta'], self.data['raw'])
        elif self.handlerType == 'sequential':
            self.backendDataService = SequentialDataHandler(self.data['meta'], self.data['raw'])
          
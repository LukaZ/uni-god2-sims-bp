#from src.data_handlers.generic_data_handler import GenericDataHandler
from src.data_handlers.data_handler import DataHandler
from src.util.logger import ActionLogger
class SubtableDataHandler(DataHandler):
    
    def __init__(self, metaData, rawData ):
        super().__init__()
        self.data = {}
        self.data['meta'] = metaData
        self.data['raw'] = rawData
        
        self.log = ActionLogger('[SubtableDataHandler]')

    def get_all(self):
        return self.data['raw']

    def get_one(self, id:int):
        return self.data['raw'][id]

    def edit(self, obj:dict, index):
        self.log.log_action("Editing value")
        self.data['raw'][index.row()] = obj
        return True
        
    def delete_one(self, id:int):
        self.log.log_action(f"Deleting {id}")
        self.data['raw'].pop(id)
        return True

    def insert(self, obj):
        self.log.log_action("Inserting new object")
        self.data['raw'].append(obj)
        return True
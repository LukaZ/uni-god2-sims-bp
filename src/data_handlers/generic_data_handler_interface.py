from abc import ABC, abstractmethod

class GenericDataHandlerInterface(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def initialize_backend_data_handler(self):
        pass

    @abstractmethod
    def isMetaDataValid(self):
        pass

    @abstractmethod
    def getMetaData(self):
        pass

    @abstractmethod
    def getMetaDataColumns(self):
        pass

    @abstractmethod
    def columnCount(self):
        pass

    @abstractmethod
    def getRowCount(self):
        pass
    
    @abstractmethod
    def insert(self, obj):
        pass


    ###
    @abstractmethod
    def get_one(self, id):
        pass
    
    @abstractmethod
    def get_all(self):
        pass

    @abstractmethod
    def edit(self, obj, index):
        pass

    @abstractmethod
    def delete_one(self, id):
        pass

    # @abstractmethod
    # def insert(self, obj):
    #     pass
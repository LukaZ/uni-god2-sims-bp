import pickle
from src.util.logger import ActionLogger
from src.data_handlers.data_handler import DataHandler
from src.util.logger import ActionLogger

class SerialDataHandler(DataHandler): #[obj, obj, obj]

    def __init__(self, metaData, rawData):
        super().__init__()
        self.data = {}
        self.data['meta'] = metaData
        self.data['raw'] = rawData

        self.log = ActionLogger("[SerialDataHandler]")

       
    def get_one(self, id:int) -> object:
        return self.data['raw'][id]
    
    def get_all(self) -> object:
        self.log.log_action("Returning all values")
        return self.data['raw']

    def edit(self, newObj, index) -> bool:
        self.log.log_action("Editing to new value")
        #match, index = self.get_object_index(oldObj)
        try:
            self.log.log_action("Setting to new value")
            self.data['raw'][index.row() if type(index) != int else index] = newObj
            return True
        except Exception as ex:
            return False

    def delete_one(self, id:int):
        self.data['raw'].pop(id)
        self.log.log_action(f"Deleted {id}")
        return True

    def insert(self, obj) -> bool:
        self.data['raw'].append(obj)
        return True
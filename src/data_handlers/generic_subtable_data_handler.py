from PySide2.QtCore import Signal, QObject

from src.data_handlers.generic_data_handler_interface import GenericDataHandlerInterface
from src.data_handlers.subtable_data_handler import SubtableDataHandler
from src.util.logger import ActionLogger

class GenericSubtableDataHandler(QObject):
    updateMasterRowData = Signal( str, int, dict )
    def _generateMetaData(self) -> dict:
        """
        Generate metadata based off the data passed in from the row since
        we dont have metadata defined for a subtable
        """
        def __obtainAllKeys() -> list:
            existingSubtableKeys = []
            for subtableRow in self.data['raw']:
                for subtableRow_Key in subtableRow:
                    if subtableRow_Key not in existingSubtableKeys:
                        existingSubtableKeys.append(subtableRow_Key)
            return existingSubtableKeys

        generatedMetadata = {}
        generatedMetadata['type'] = 'subtable'
        generatedMetadata['sourceFile'] = 'N/A' # Just so we don't fail the check
        generatedMetadata['columns'] = __obtainAllKeys()

        isMetaValid, _missingKey = self.isMetaDataValid(generatedMetadata)
        if not isMetaValid:
            raise Exception(f"Invalid generated metadata, missing key \"{_missingKey}\"")

        return generatedMetadata
        

    def __init__(self, rowData:dict, masterRowID:int, subtablePK:str):
        """
        rowData = the subtable data conatined in the parrent table 
        masterRowID = the id of the master row in the main table
        subtablePK = the string identifying which subtable we are 
            working with ( One contained in main metadata under 'subtables' )
        """
        
        super().__init__()
        self.log = ActionLogger('[GenericSubtableDataHandler]')
        
        self.subtablePK = subtablePK
        self.masterRowID = masterRowID

        
        self.data = {}
        self.data['raw'] = rowData if type(rowData) == list else [rowData,] # Support multiple subtables
        self.data['meta'] = self._generateMetaData()
        
        self.initialize_backend_data_handler()
    

    def initialize_backend_data_handler(self):
        self.log.log_action('Initializing backend data handler')
        self.backendDataService = SubtableDataHandler(self.data['meta'], self.data['raw'])


    def isMetaDataValid(self, metaData):
        _requiredKeys=['type', 'sourceFile', 'columns' ]
        for key in _requiredKeys:
            if key not in metaData:
                return False, key
        return True, ""

    def getMetaData(self):
        return self.data['meta']

    # We assume that the first row of the subtable has all the 
    def getMetaDataColumns(self): 
        return list(self.data['meta']['columns'])

    def columnCount(self):
        return len(self.getMetaDataColumns())

    def getRowCount(self):
        return len(self.data['raw'])
    
    def get_one(self, id):
        return self.backendDataService.get_one(id) 
    
    def get_all(self):
        return self.backendDataService.get_all()

    def insert(self, obj):
        self.backendDataService.insert(obj)
        self.save_rawdata()
        return True

    def edit(self, obj, index):

        self.backendDataService.edit(obj,index)
        self.save_rawdata(index)
        return True

    def delete_one(self, id):
        result = self.backendDataService.delete_one(id)
        if result:
            self.save_rawdata()
            return True
        return False

    def save_rawdata(self, index=None):
        """
        Save the subtable that's located inside a master table
        """
        index = len(self.backendDataService.get_all()) + 1 if index==None else index # new or existing ?
        self.updateMasterRowData.emit( self.subtablePK, index, self.get_all() )

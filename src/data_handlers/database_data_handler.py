from PySide2.QtSql import QSqlTableModel, QSqlRelationalTableModel, QSqlRelation, QSqlQuery
from PySide2.QtCore import Qt
from src.data_handlers.data_handler import DataHandler

from src.util.logger import ActionLogger

class DatabaseDataHandler(QSqlRelationalTableModel):
    
    def getTableDisplayCol(self, tableName:str):
        tableMeta = self.masterHandler.getTableMetaData(tableName)
        displayCol = None
        for colName in tableMeta:
            col = tableMeta[colName]
            if col['dataType'] == 'text' or col['dataType'] == 'character varying':
                displayCol = colName
                break
        return displayCol

    def __init__(self, table:str, foreignRelations:dict, tableColumnsMeta:dict, masterHandler):
        super().__init__()
        self.log = ActionLogger('[DatabaseDataHandler]')

        self.masterHandler = masterHandler
        self.tableName = table
        self.setTable(table)
        
        self.setQuery(QSqlQuery(f'SELECT oid, * FROM {table}'))
        self.setEditStrategy(QSqlTableModel.OnFieldChange)
        
        # for relation in foreignRelations:
        #     number = tableColumnsMeta[relation['constraintName']]['numericIndex']
            
        #     displayCol = self.getTableDisplayCol(relation['foreignTableName'])
        #     if displayCol == None:
        #         print("Here")
        #     self.setRelation(
        #         number,
        #         QSqlRelation(
        #             relation['foreignTableName'],
        #             relation['foreignColumnName'],
        #             displayCol
        #         )
        #     )
        
        self.select()
        self.log.log_action("Initialized!")

    def save_rawdata(self):
        return True 

    def delete_one(self, oid:int):
        self.log.log_action(f"Deleting row with OID {oid} one...")
        return self.masterHandler.delete_one(int(oid), self.tableName)
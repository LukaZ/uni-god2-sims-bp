from PySide2 import QtWidgets, QtGui, QtCore

from src.widgets.main_window import MainWindow
from forms.ui_main import Ui_MainWindow

if __name__ == '__main__':
    app = QtWidgets.QApplication([]) # Start an application.
    
    mainWindow = MainWindow()

    ui = Ui_MainWindow()
    ui.setupUi(mainWindow)
    
    mainWindow.setupUi(ui)
    mainWindow.show() # Show window


    app.exec_() # Execute the App



DROP TABLE IF EXISTS public.korisnici;

CREATE TABLE public.korisnici
(
    id serial NOT NULL,
    korisnicko_ime character varying(60) NOT NULL,
    lozinka character varying(90) NOT NULL,
    email character varying(90),
    PRIMARY KEY (id)
) WITH OIDS;

ALTER TABLE public.korisnici
    OWNER to postgres;



/* DOMACI */


CREATE TABLE public.predmeti
(
    id serial NOT NULL,
    naziv character varying(60) NOT NULL,
    akronim character varying(24) NOT NULL,
    semestar integer,
    espb integer NOT NULL,
    PRIMARY KEY (id)
) WITH OIDS;

ALTER TABLE public.predmeti
    OWNER to postgres;

DROP TABLE IF EXISTS public.domaci;

CREATE TABLE public.domaci
(
    id serial NOT NULL,
    predmetid integer NOT NULL,
    opis text NOT NULL,
    korisnikid integer NOT NULL,
    CONSTRAINT domaci_pk PRIMARY KEY (id),
    CONSTRAINT korisnikid FOREIGN KEY (korisnikid)
        REFERENCES public.korisnici (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,

    CONSTRAINT predmetid FOREIGN KEY (predmetid)
        REFERENCES public.predmeti (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.domaci
    OWNER to postgres;


/* ISPITI */
CREATE TABLE public.ispiti
(
    id serial NOT NULL,
    predmetid integer NOT NULL,
    opis text,
    rok date NOT NULL,
    korisnikid integer NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT korisnikid FOREIGN KEY (korisnikid)
        REFERENCES public.korisnici (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,

    CONSTRAINT predmetid FOREIGN KEY (predmetid)
        REFERENCES public.predmeti (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH OIDS;

ALTER TABLE public.ispiti
    OWNER to postgres;




/* GENERISANJE PODATAKA */

INSERT INTO public.korisnici( korisnicko_ime, email, lozinka) VALUES ('luka', 'nobody@nowhere.com','123');
INSERT INTO public.korisnici( korisnicko_ime, email, lozinka) VALUES ('pera', 'pera@nowhere.com','321');

INSERT INTO public.predmeti( naziv, akronim, semestar, espb) VALUES ('Biologija', 'BIO', 1, 8);
INSERT INTO public.predmeti( naziv, akronim, semestar, espb) VALUES ('Matematika', 'MAT', 3, 6);
INSERT INTO public.predmeti( naziv, akronim, semestar, espb) VALUES ('Kriptologija 1', 'KRIPT1', 1, 6);
INSERT INTO public.predmeti( naziv, akronim, semestar, espb) VALUES ('Kriminalistika 1', 'KRIM1', 1, 6);
INSERT INTO public.predmeti( naziv, akronim, semestar, espb) VALUES ('Vestacka inteligencija', 'VEIN', 1, 6);

INSERT INTO public.domaci( predmetid, opis, korisnikid) VALUES (1,'Zavrsiti herbalijum',1);
INSERT INTO public.domaci( predmetid, opis, korisnikid) VALUES (2,'Zavrsiti nacrt',1);
INSERT INTO public.domaci( predmetid, opis, korisnikid) VALUES (3,'Zavrsiti prezentaciju za AES256',1);
INSERT INTO public.domaci( predmetid, opis, korisnikid) VALUES (4,'Zavrsiti izvestaj za Q1/2019',2);
INSERT INTO public.domaci( predmetid, opis, korisnikid) VALUES (2,'Ponovo uciti za ispit',2);
INSERT INTO public.domaci( predmetid, opis, korisnikid) VALUES (5,'Novi algoritam uraditi',2);

INSERT INTO public.ispiti( predmetid, opis, rok, korisnikid) VALUES (1,'Ispit vezan za celije', '2020-09-01', 1);
INSERT INTO public.ispiti( predmetid, opis, rok, korisnikid) VALUES (2,'Algebra 2' , '2020-03-29', 1);
INSERT INTO public.ispiti( predmetid, opis, rok, korisnikid) VALUES (4,'Detekcija lica i generisanje jedinstvenog ID-ja' ,'2020-06-15', 1);
INSERT INTO public.ispiti( predmetid, opis, rok, korisnikid) VALUES (5,'Osnovni AI za igricu' ,'2020-09-07', 2);

/*
CREATE OR REPLACE FUNCTION GetTableForeignKeys( tableToSearch character varying )
LANGUAGE 'internal'
AS $$
BEGIN
    SELECT DISTINCT ON ( kcu.constraint_name )
    tc.table_schema, 
    tc.constraint_name, 
    tc.table_name, 
    kcu.column_name, 
    ccu.table_schema AS foreign_table_schema,
    ccu.table_name AS foreign_table_name,
    ccu.column_name AS foreign_column_name 
    FROM 
        information_schema.table_constraints AS tc 
        JOIN information_schema.key_column_usage AS kcu
            ON tc.constraint_name = kcu.constraint_name
            AND tc.table_schema = kcu.table_schema
        JOIN information_schema.constraint_column_usage AS ccu
            ON ccu.constraint_name = tc.constraint_name
            AND ccu.table_schema = tc.table_schema
    WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=tableToSearch;
*/

/*
SELECT DISTINCT ON ( kcu.constraint_name )
    tc.table_schema, 
    tc.constraint_name, 
    tc.table_name, 
    kcu.column_name, 
    ccu.table_schema AS foreign_table_schema,
    ccu.table_name AS foreign_table_name,
    ccu.column_name AS foreign_column_name 
FROM 
    information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
      ON tc.constraint_name = kcu.constraint_name
      AND tc.table_schema = kcu.table_schema
    JOIN information_schema.constraint_column_usage AS ccu
      ON ccu.constraint_name = tc.constraint_name
      AND ccu.table_schema = tc.table_schema
WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name='ispiti';
*/
#!/bin/bash

echo -n "Izaberite opciju:
    1.Pokreni PostgreSQL
    2.Pokreni i ocistiti podatke
: "
read OPTION

if [ $OPTION -eq 1 ] 
then
  sudo docker-compose up
else
  sudo docker-compose down --volumes
  sudo docker-compose up
  # docker exec -i rs_02_main pg_restore -U postgres < /home/luka/Desktop/GIT_FILES/FAKS/uni-god2-sims-bp/docker/dvdrental.tar
fi

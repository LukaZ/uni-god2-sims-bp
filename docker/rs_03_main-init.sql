CREATE TABLE public.VisokoskolskaUstanova
(
    oznaka character(2) NOT NULL,
    naziv character varying(80),
    adresa character varying(80),
    PRIMARY KEY (oznaka)
) WITH OIDS;
ALTER TABLE public.VisokoskolskaUstanova 
    OWNER to postgres;



CREATE TABLE public.NivoStudija
(
    oznaka numeric(2, 0) NOT NULL,
    naziv character varying(80),
    PRIMARY KEY (oznaka)
) WITH OIDS;
ALTER TABLE public.NivoStudija
    OWNER to postgres;


CREATE TABLE public.NastavniPredmet
(
    ustanova character(2) NOT NULL,
    oznaka character varying(6) NOT NULL,
    naziv character varying(120),
    espb numeric(2, 0),
    PRIMARY KEY (ustanova, oznaka),
    
    CONSTRAINT ustanova FOREIGN KEY (ustanova)
        REFERENCES public.VisokoskolskaUstanova (oznaka) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) WITH OIDS;
ALTER TABLE public.NastavniPredmet
    OWNER to postgres;



CREATE TABLE public.StudijskiProgrami
(
    ustanova character(2) NOT NULL,
    nivo numeric(2, 0),
    oznaka_programa character varying(3) NOT NULL,
    naziv_programa character varying(120),
    PRIMARY KEY (ustanova, oznaka_programa),

    CONSTRAINT ustanova FOREIGN KEY (ustanova)
        REFERENCES public.VisokoskolskaUstanova (oznaka) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    
    CONSTRAINT nivo FOREIGN KEY (nivo)
        REFERENCES public.NivoStudija (oznaka) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) WITH OIDS;
ALTER TABLE public.StudijskiProgrami
    OWNER to postgres;



CREATE TABLE public.PlanStudijskeGrupe
(
    program_ustanove character(2) NOT NULL,
    oznaka_programa character varying(3) NOT NULL,
    blok numeric(2, 0) NOT NULL,
    pozicija numeric(2, 0) NOT NULL,
    ustanova_predmet character(2),
    oznaka_predmet character varying(6),
    
    PRIMARY KEY (program_ustanove, oznaka_programa, blok, pozicija),

    CONSTRAINT program_ustanove FOREIGN KEY (program_ustanove, oznaka_programa)
        REFERENCES public.StudijskiProgrami (ustanova, oznaka_programa) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,

    CONSTRAINT ustanova_predmet FOREIGN KEY (ustanova_predmet, oznaka_predmet)
        REFERENCES public.NastavniPredmet (ustanova, oznaka) MATCH SIMPLE
        ON UPDATE CASCADE      
        ON DELETE CASCADE
) WITH OIDS;
ALTER TABLE public.PlanStudijskeGrupe
    OWNER to postgres;



CREATE TABLE public.Studenti
(
    ustanova character(2) NOT NULL,
    struka character(2) NOT NULL,
    broj_indeksa character varying(6) NOT NULL,
    prezime character varying(20),
    ime_roditelja character varying(20),
    ime character varying(20),
    pol character(1),
    adresa_stanovanja character varying(80),
    telefon character varying(20),
    jmbg character(13),
    datum_rodjenja date,

    PRIMARY KEY (ustanova, struka, broj_indeksa),

    CONSTRAINT ustanova FOREIGN KEY (ustanova)
        REFERENCES public.VisokoskolskaUstanova (oznaka) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) WITH OIDS;
ALTER TABLE public.Studenti
    OWNER to postgres;



CREATE TABLE public.TokStudija
(
    ustanova character(2) NOT NULL,
    oznaka_programa character varying(3) NOT NULL,
    student_iz_ustanove character(2) NOT NULL,
    struka character(2) NOT NULL,
    broj_indeksa character varying(6) NOT NULL,
    skolska_godina numeric(4,0) NOT NULL,
    godina_studija numeric(1,0) NOT NULL,
    blok numeric(2,0) NOT NULL,
    redni_broj_upisa numeric(2,0) NOT NULL,
    datum_upisa date,
    datum_overe date,
    espb_pocetni numeric(3,0),
    espb_krajnji numeric(3,0),

    PRIMARY KEY (skolska_godina, godina_studija, blok, redni_broj_upisa),

    CONSTRAINT ustanova FOREIGN KEY (ustanova, oznaka_programa)
        REFERENCES public.StudijskiProgrami (ustanova, oznaka_programa) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    
    CONSTRAINT student_iz_ustanove FOREIGN KEY (student_iz_ustanove, struka, broj_indeksa)
        REFERENCES public.Studenti (ustanova, struka, broj_indeksa) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) WITH OIDS;
ALTER TABLE public.TokStudija
    OWNER to postgres;



INSERT INTO public.VisokoskolskaUstanova(oznaka, naziv, adresa) VALUES ('MF', 'Medicinski Fakultet', 'Bulevar oslobodjenja 8');
INSERT INTO public.VisokoskolskaUstanova(oznaka, naziv, adresa) VALUES ('SI', 'Singidunum', 'Danijelova 32, Beograd 160622');
INSERT INTO public.VisokoskolskaUstanova(oznaka, naziv, adresa) VALUES ('SF', 'Sportski Fakultet', 'Dr Subotića starijeg 8, Bograd');

INSERT INTO public.NivoStudija(oznaka, naziv) VALUES (32, 'Osnovne');
INSERT INTO public.NivoStudija(oznaka, naziv) VALUES (64, 'Master');
INSERT INTO public.NivoStudija(oznaka, naziv) VALUES (96, 'Doktorske');

INSERT INTO public.NastavniPredmet(ustanova, oznaka, naziv, espb) VALUES ('MF', 'HIRURG', 'Osnove Hirurgije 1', 12);
INSERT INTO public.NastavniPredmet(ustanova, oznaka, naziv, espb) VALUES ('SI', 'SIMSBP', 'Strukture i modeliranje softvera & Baze podataka', 16);
INSERT INTO public.NastavniPredmet(ustanova, oznaka, naziv, espb) VALUES ('SF', 'OSSKUV', 'Osnove skakanja u vis', 6);

INSERT INTO public.StudijskiProgrami(ustanova, nivo, oznaka_programa, naziv_programa) 
    VALUES ('MF', 32, 'HIR', 'Hirurgija 1');
INSERT INTO public.StudijskiProgrami(ustanova, nivo, oznaka_programa, naziv_programa) 
   VALUES ('SI', 64, 'OP2', 'Objektno orijentisano programiranje 2');
INSERT INTO public.StudijskiProgrami(ustanova, nivo, oznaka_programa, naziv_programa) 
   VALUES ('SF', 32, 'ATL', 'Atletika');

INSERT INTO public.PlanStudijskeGrupe( program_ustanove, oznaka_programa, blok, pozicija, ustanova_predmet, oznaka_predmet)
	VALUES ('MF', 'HIR', 10, 1, 'MF', 'HIRURG');
INSERT INTO public.PlanStudijskeGrupe( program_ustanove, oznaka_programa, blok, pozicija, ustanova_predmet, oznaka_predmet)
	VALUES ('SI', 'OP2', 4, 32, 'SI', 'SIMSBP');
INSERT INTO public.PlanStudijskeGrupe( program_ustanove, oznaka_programa, blok, pozicija, ustanova_predmet, oznaka_predmet)
	VALUES ('SF', 'ATL', 2, 15, 'SF', 'OSSKUV');

INSERT INTO public.Studenti(
	ustanova, struka, broj_indeksa, prezime, ime_roditelja, ime, pol, adresa_stanovanja, telefon, jmbg, datum_rodjenja)
	VALUES (
    'MF', 'BE', '241551', 'Masic', 'Ivan', 'Maja', 'M', 'Bulevar Banana 8, 2222', '062123456', '2221112324569', '1992-03-14'
);
INSERT INTO public.Studenti(
	ustanova, struka, broj_indeksa, prezime, ime_roditelja, ime, pol, adresa_stanovanja, telefon, jmbg, datum_rodjenja)
	VALUES (
    'SI', 'SE', '270166', 'Zagar', 'Vilko', 'Luka', 'M', 'Bulevar Kaktusa 2, 4523', '111222333', '2521712394499', '1999-03-30'
);
INSERT INTO public.Studenti(
	ustanova, struka, broj_indeksa, prezime, ime_roditelja, ime, pol, adresa_stanovanja, telefon, jmbg, datum_rodjenja)
	VALUES (
    'SF', 'LT', '131751', 'Igor', 'Paja', 'Igor', 'Z', 'Neka Ulica 9, 0000', '0000000001', '2241112325542', '1942-02-15'
);

INSERT INTO public.TokStudija(
    ustanova,
    oznaka_programa,
    student_iz_ustanove,
    struka,
    broj_indeksa,
    skolska_godina,
    godina_studija,
    blok,
    redni_broj_upisa,
    datum_upisa,
    datum_overe,
    espb_pocetni,
    espb_krajnji
) VALUES (
    'SI', 'OP2', 'SI', 'SE', '270166', 2019, 4, 12, 1, '2019-01-01', '2020-02-15', 240, 240);